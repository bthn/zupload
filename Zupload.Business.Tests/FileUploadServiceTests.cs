using System;
using System.IO.Abstractions.TestingHelpers;
using System.Threading.Tasks;
using FluentValidation;
using Microsoft.EntityFrameworkCore;
using Microsoft.Extensions.Logging;
using Moq;
using Xunit;
using Zupload.Business.Abstract;
using Zupload.Business.Concrete;
using Zupload.DataAccess;
using Zupload.DTOs;

namespace Zupload.Business.Tests
{
    public class FileUploadServiceTests
    {
        private readonly DbContextOptions<ZuploadContext> _dbContextOptions;
        private readonly Mock<IFileSaveService> _saveServiceMock = new();
        private readonly Mock<ILogger<FileUploadService>> _loggerMock = new();
        

        public FileUploadServiceTests()
        {
            _dbContextOptions = new DbContextOptionsBuilder<ZuploadContext>()
                .UseInMemoryDatabase("zupload")
                .Options;
        }

        [Fact]
        public void UploadFileWithExtension_ReturnsMetaData()
        {
            using var context = new ZuploadContext(_dbContextOptions);
            var mockFileSystem = new MockFileSystem();
            var mockInputFile = new MockFileData("line1\nline2\nline3");
            const string filename = "sample.txt";
            mockFileSystem.AddFile(filename, mockInputFile);
            var bytes = mockFileSystem.File.ReadAllBytes(filename);
                
            var service = new FileUploadService(_loggerMock.Object,
                _saveServiceMock.Object, context);
            var model = new UploadFileModel()
            {
                ProvidedFileName = filename,
                Content = bytes
            };
            var result = service.UploadFileAsync(model).Result;
            Assert.Equal(filename, result.FileName);
        }
        
        [Fact]
        public async Task UploadFileWithoutExtension_ThrowsError()
        {
            await using var context = new ZuploadContext(_dbContextOptions);
            var mockFileSystem = new MockFileSystem();
            var mockInputFile = new MockFileData("line1\nline2\nline3");
            const string filename = "sample";
            mockFileSystem.AddFile(filename, mockInputFile);
            var bytes = await mockFileSystem.File.ReadAllBytesAsync(filename);
                
            var service = new FileUploadService(_loggerMock.Object,
                _saveServiceMock.Object, context);
            var model = new UploadFileModel()
            {
                ProvidedFileName = filename,
                Content = bytes
            };
            await Assert.ThrowsAnyAsync<ValidationException>(() => service.UploadFileAsync(model));
        }
        
        [Fact]
        public async Task UploadFileWithLongExtension_ThrowsError()
        {
            await using var context = new ZuploadContext(_dbContextOptions);
            var mockFileSystem = new MockFileSystem();
            var mockInputFile = new MockFileData("line1\nline2\nline3");
            var filename = "sample." + Guid.NewGuid().ToString();
            mockFileSystem.AddFile(filename, mockInputFile);
            var bytes = await mockFileSystem.File.ReadAllBytesAsync(filename);
                
            var service = new FileUploadService(_loggerMock.Object,
                _saveServiceMock.Object, context);
            var model = new UploadFileModel()
            {
                ProvidedFileName = filename,
                Content = bytes
            };
            await Assert.ThrowsAnyAsync<ValidationException>(() => service.UploadFileAsync(model));
        }
    }
}