using System.Threading.Tasks;

namespace Zupload.Business.Abstract
{
    public interface IFileSaveService
    {
        // returns save path
        Task<string> SaveFileAsync(byte[] data, string fileName);
    }
}