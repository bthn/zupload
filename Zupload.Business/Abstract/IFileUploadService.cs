using System.Threading.Tasks;
using Zupload.Core.Pagination;
using Zupload.DTOs;
using Zupload.Entities;

namespace Zupload.Business.Abstract
{
    public interface IFileUploadService
    {
        Task<FileMetaDataModel> UploadFileAsync(UploadFileModel uploadFileModel);
        Task<PaginationResponseModel<FileMetaDataModel>> GetPaginated(PaginationRequestModel model);
    }
}