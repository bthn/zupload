using System.Threading.Tasks;

namespace Zupload.Business.Abstract
{
    public interface IScanService
    {
        Task<bool> Scan(byte[] content);
    }
}