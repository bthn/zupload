using System;
using System.IO;
using System.Threading.Tasks;
using Microsoft.Extensions.Configuration;
using Zupload.Business.Abstract;

namespace Zupload.Business.Concrete
{
    public class DiskSaveManager : IFileSaveService
    {
        private readonly string _targetFilePath;

        public DiskSaveManager(IConfiguration config)
        {
            // To save physical files to a path provided by configuration:
            _targetFilePath = config.GetSection("StoredFilesPath").Value;
        }
        
        public async Task<string> SaveFileAsync(byte[] data, string fileName)
        {
            var trustedFileNameForFileStorage = 
                Guid.NewGuid().ToString()
                + Path.GetExtension(fileName);
            
            await using var targetStream = System.IO.File.Create(
                Path.Combine(_targetFilePath, trustedFileNameForFileStorage));
            
            await targetStream.WriteAsync(data);
            
            return targetStream.Name;
        }
    }
}