using System.Threading.Tasks;
using Zupload.Business.Abstract;

namespace Zupload.Business.Concrete
{
    public class DumbVirusScanner : IScanService
    {
        public async Task<bool> Scan(byte[] content)
        {
            return await Task.FromResult(true);
        }
    }
}