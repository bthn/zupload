using System;
using System.IO;
using System.Linq;
using System.Net;
using System.Threading.Tasks;
using Microsoft.EntityFrameworkCore;
using Microsoft.Extensions.Logging;
using Zupload.Business.Abstract;
using Zupload.Business.ValidationRules.FluentValidation;
using Zupload.Core.CrossCuttingConcerns.Validation.FluentValidation;
using Zupload.Core.Pagination;
using Zupload.DataAccess;
using Zupload.DTOs;
using Zupload.Entities;

namespace Zupload.Business.Concrete
{
    public class FileUploadService : IFileUploadService
    {
        private readonly ILogger<FileUploadService> _logger;
        private readonly IFileSaveService _fileSaveService;
        private readonly ZuploadContext _context;

        public FileUploadService(ILogger<FileUploadService> logger,
            IFileSaveService fileSaveService, ZuploadContext context)
        {
            _fileSaveService = fileSaveService;
            _logger = logger;
            _context = context;
        }
        
        //didn't use aspect oriented approach here for validation
        public async Task<FileMetaDataModel> UploadFileAsync(UploadFileModel uploadFileModel)
        {
            ValidatorTool<UploadFileModel>.Validate(new FileUploadValidator(), uploadFileModel);
            
            // Don't trust the file name sent by the client. To display
            // the file name, HTML-encode the value.
            var trustedFileNameForDisplay = WebUtility.HtmlEncode(
                uploadFileModel.ProvidedFileName);
            
            var ext = Path.GetExtension(trustedFileNameForDisplay);

            var fileType = await _context.FileTypes
                .Where(e => e.Extension == ext)
                .FirstOrDefaultAsync() ?? new FileType()
            {
                Extension = ext
            };

            var savePath = await _fileSaveService.SaveFileAsync(uploadFileModel.Content,
                trustedFileNameForDisplay);

            _logger.LogInformation(
                "Uploaded file '{TrustedFileNameForDisplay}' saved as " +
                "'{TrustedFileNameForFileStorage}'", 
                trustedFileNameForDisplay, savePath);

            var record = new FileMetaData()
            {
                FileName = trustedFileNameForDisplay,
                FileType = fileType,
                UploadDate = DateTime.UtcNow,
                StoragePath = savePath,
                FileSizeInBytes = uploadFileModel.Content.Length
            };

            var entry = _context.Add(record);

            await _context.SaveChangesAsync();

            return new FileMetaDataModel()
            {
                Id = entry.Entity.Id,
                FileName = entry.Entity.FileName,
                UploadDate = entry.Entity.UploadDate,
                FileSizeInBytes = entry.Entity.FileSizeInBytes
            };
        }

        public async Task<PaginationResponseModel<FileMetaDataModel>> GetPaginated(PaginationRequestModel model)
        {
            ValidatorTool<PaginationRequestModel>.Validate(new FilePaginationValidator(), model);
            
            var totalCount = await _context.FileMetaData.CountAsync();
            var skipAmount = (model.PageNumber - 1) * model.PageSize;
            var data = await _context.FileMetaData
                .OrderByDescending(e => e.UploadDate)
                .Skip(skipAmount)
                .Take(model.PageSize)
                .Select(e => new FileMetaDataModel()
                {
                    Id = e.Id,
                    FileName = e.FileName,
                    UploadDate = e.UploadDate,
                    FileSizeInBytes = e.FileSizeInBytes
                })
                .ToListAsync();

            var pageCount = totalCount / model.PageSize;
            if (totalCount % model.PageSize > 0)
            {
                pageCount++;
            }

            return new PaginationResponseModel<FileMetaDataModel>()
            {
                Data = data,
                PageCount = pageCount 
            };
        }
    }
}