using FluentValidation;
using Zupload.Core.Pagination;

namespace Zupload.Business.ValidationRules.FluentValidation
{
    public class FilePaginationValidator : AbstractValidator<PaginationRequestModel>
    {
        public FilePaginationValidator()
        {
            RuleFor(e => e.PageNumber).GreaterThan(0);
            RuleFor(e => e.PageSize).InclusiveBetween(1, 100);
        }
    }
}