using System.IO;
using System.Net;
using FluentValidation;
using Zupload.DTOs;

namespace Zupload.Business.ValidationRules.FluentValidation
{
    public class FileUploadValidator : AbstractValidator<UploadFileModel>
    {
        public FileUploadValidator()
        {
            RuleFor(x => x.Content).NotEmpty();
            RuleFor(x => x.ProvidedFileName).Length(1, 200);
            RuleFor(x => x.ProvidedFileName).Must(ValidExtension);
        }

        private bool ValidExtension(string filename)
        {
            var encodedFilename = WebUtility.HtmlEncode(filename);
            
            var ext = Path.GetExtension(encodedFilename);
            
            return !string.IsNullOrWhiteSpace(ext)
                && ext.Length > 1
                && ext.Length <= 30;
        }
    }
}