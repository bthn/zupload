using FluentValidation;

namespace Zupload.Core.CrossCuttingConcerns.Validation.FluentValidation
{
    public static class ValidatorTool<T>
    {
        public static void Validate(IValidator<T> validator, T model)
        {
            var result = validator.Validate(model);

            if (result.Errors.Count > 0)
            {
                throw new ValidationException(result.Errors);
            }
        }
    }
}