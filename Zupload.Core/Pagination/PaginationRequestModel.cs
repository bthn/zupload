namespace Zupload.Core.Pagination
{
    public class PaginationRequestModel
    {
        public int PageNumber { get; set; }
        public int PageSize { get; set; }
    }
}