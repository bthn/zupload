using System.Collections.Generic;

namespace Zupload.Core.Pagination
{
    public class PaginationResponseModel<T> where T : class
    {
        public List<T> Data { get; set; }
        public int PageCount { get; set; }
    }
}