using System;

namespace Zupload.DTOs
{
    public class FileMetaDataModel
    {
        public long Id { get; set; }
        public string FileName { get; set; }
        public long FileSizeInBytes { get; set; }
        public DateTime UploadDate { get; set; }
    }
}