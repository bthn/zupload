﻿using System;

namespace Zupload.DTOs
{
    public class UploadFileModel
    {
        public string ProvidedFileName { get; set; }
        public byte[] Content { get; set; }
    }
}