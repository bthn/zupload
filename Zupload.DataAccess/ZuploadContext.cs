using Microsoft.EntityFrameworkCore;
using Microsoft.EntityFrameworkCore.Metadata;
using Zupload.Entities;

namespace Zupload.DataAccess
{
    public class ZuploadContext : DbContext
    {
        
        /* Migration procedure:
         * 1. open terminal in DataAccess project folder
         * 2. create a migration script: dotnet ef migrations add <NameOfMigration>
         * 3. apply migration: dotnet ef database update
         */
        
        public DbSet<FileMetaData> FileMetaData { get; set; }
        public DbSet<FileType> FileTypes { get; set; }

        private readonly bool inMemory;

        public ZuploadContext()
        {
            inMemory = false;
        }

        public ZuploadContext(DbContextOptions<ZuploadContext> dbContextOptions) : base(dbContextOptions)
        {
            inMemory = true;
        }
        
        protected override void OnConfiguring(DbContextOptionsBuilder optionsBuilder)
        {
            if (inMemory)
            {
                base.OnConfiguring(optionsBuilder);
            }
            else
            {
                optionsBuilder.UseNpgsql(
                    "Host=localhost;Database=zupload;Username=batuhanbulut;Password=zudo"
                );
            }
        }

        protected override void OnModelCreating(ModelBuilder modelBuilder)
        {
            //Override default naming convention which uses plural names. Use singular.
            foreach (IMutableEntityType entityType in modelBuilder.Model.GetEntityTypes())
            {
                entityType.SetTableName(entityType.DisplayName());
            }

            modelBuilder.Entity<FileType>()
                .HasIndex(e => e.Extension)
                .IsUnique();

            modelBuilder.Entity<FileMetaData>()
                .HasIndex(e => e.UploadDate);
        }
    }
}