using System;
using System.ComponentModel.DataAnnotations;
using Zupload.Core;

namespace Zupload.Entities
{
    public class FileMetaData : IEntity
    {
        public long Id { get; set; }
        [MaxLength(200)]
        public string FileName { get; set; }
        public int FileTypeId { get; set; }
        public FileType FileType { get; set; }
        public string StoragePath { get; set; }
        public long FileSizeInBytes { get; set; }
        public DateTime UploadDate { get; set; }
    }
}