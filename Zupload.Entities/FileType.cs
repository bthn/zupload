using System.Collections.Generic;
using System.ComponentModel.DataAnnotations;
using Zupload.Core;

namespace Zupload.Entities
{
    public class FileType : IEntity
    {
        public int Id { get; set; }
        [MaxLength(30)]
        public string Extension { get; set; }

        public ICollection<FileMetaData> FileMetaData { get; set; }
    }
}