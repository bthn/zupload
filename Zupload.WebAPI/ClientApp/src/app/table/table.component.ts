import { Component, OnInit } from '@angular/core';
import { HttpClient } from '@angular/common/http';

@Component({ templateUrl: 'table.component.html' })
export class TableComponent implements OnInit {
  files = [];
  pager = {
    currentPage: 0,
    pages: [],
    totalPages: 0
  };
  fileData: File = null;

  constructor(
    private http: HttpClient,
  ) { }

  ngOnInit() {
    this.loadPage(1);
  }

  loadPage(page) {
    this.http.post<any>(`/api/file/getpaginated`, {
      'pageNumber': page,
      'pageSize': 5
    }).subscribe(x => {
      this.files = x.data;
      this.files.map(i => i.uploadDate = new Date(Date.parse(i.uploadDate + "Z")))
      this.pager.currentPage = page;
      this.pager.pages = [...Array(x.pageCount).keys()].map(i => i+1);
      this.pager.totalPages = x.pageCount;
    });
  }

  onFileSelect(event) {
    if (event.target.files.length > 0) {
      this.fileData = <File>event.target.files[0];
    }
  }

  onSubmit() {
    const formData = new FormData();
    console.log(this.fileData);
    formData.append('file', this.fileData);
    this.http.post('/api/file', formData)
      .subscribe(res => {
        alert('SUCCESS !!');
        this.loadPage(this.pager.currentPage);
      }, error => {
        console.log(error);
        alert(error.error);
      })
  }
}
