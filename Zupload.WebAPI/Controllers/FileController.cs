using System;
using System.Collections.Generic;
using System.Globalization;
using System.IO;
using System.Threading.Tasks;
using Microsoft.AspNetCore.Http;
using Microsoft.AspNetCore.Http.Features;
using Microsoft.AspNetCore.Mvc;
using Microsoft.AspNetCore.Mvc.ModelBinding;
using Microsoft.AspNetCore.WebUtilities;
using Microsoft.Extensions.Configuration;
using Microsoft.Extensions.Logging;
using Microsoft.Net.Http.Headers;
using Zupload.Business.Abstract;
using Zupload.Core.Pagination;
using Zupload.Core.Utilities;
using Zupload.DTOs;

namespace Zupload.WebAPI.Controllers
{
    [ApiController]
    [Route("api/[controller]")]
    public class FileController : ControllerBase
    {
        // Get the default form options so that we can use them to set the default 
        // limits for request body data.
        private static readonly FormOptions DefaultFormOptions = new FormOptions();
        private readonly ILogger<FileController> _logger;
        private readonly string[] _permittedExtensions;
        private readonly long _fileSizeLimit;
        // private readonly string _targetFilePath;
        private readonly IFileUploadService _fileUploadService;
        private readonly IScanService _scanService;

        public FileController(ILogger<FileController> logger,
            IConfiguration config, IFileUploadService fileUploadService,
            IScanService scanService)
        {
            _logger = logger;
            _fileSizeLimit = config.GetValue<long>("FileSizeLimit");
            _fileUploadService = fileUploadService;
            _scanService = scanService;
            var permittedExtensions = new List<string>();
            config.GetSection("PermittedExtensions").Bind(permittedExtensions);
            _permittedExtensions = permittedExtensions.ToArray();
        }

        [HttpPost]
        public async Task<ActionResult> Post()
        {
            if (!MultipartRequestHelper.IsMultipartContentType(Request.ContentType))
            {
                // Log error
                return BadRequest("The request couldn't be processed.");
            }

            // Accumulate the form data key-value pairs in the request (formAccumulator).
            var formAccumulator = new KeyValueAccumulator();

            var boundary = MultipartRequestHelper.GetBoundary(
                MediaTypeHeaderValue.Parse(Request.ContentType),
                DefaultFormOptions.MultipartBoundaryLengthLimit);
            var reader = new MultipartReader(boundary, HttpContext.Request.Body);

            var section = await reader.ReadNextSectionAsync();
            var formData = new UploadFileModel();
            
            while (section != null)
            {
                var hasContentDispositionHeader = 
                    ContentDispositionHeaderValue.TryParse(
                        section.ContentDisposition, out var contentDisposition);

                if (hasContentDispositionHeader)
                {
                    if (MultipartRequestHelper
                        .HasFileContentDisposition(contentDisposition))
                    {
                        formData.ProvidedFileName = contentDisposition.FileName.Value;

                        try
                        {
                            formData.Content = 
                                await FileHelpers.ProcessStreamedFile(section, contentDisposition, 
                                    _permittedExtensions, _fileSizeLimit);
                        }
                        catch (Exception e)
                        {
                            return BadRequest(e.Message);
                        }
                        
                    }
                    else if (MultipartRequestHelper
                        .HasFormDataContentDisposition(contentDisposition))
                    {
                        // Don't limit the key name length because the 
                        // multipart headers length limit is already in effect.
                        var key = HeaderUtilities
                            .RemoveQuotes(contentDisposition.Name).Value;
                        var encoding = FileHelpers.GetEncoding(section);
                    
                        if (encoding == null)
                        {
                            // Log error
                    
                            return BadRequest($"The request couldn't be processed (Error 2).");
                        }
                    
                        using (var streamReader = new StreamReader(
                            section.Body,
                            encoding,
                            detectEncodingFromByteOrderMarks: true,
                            bufferSize: 1024,
                            leaveOpen: true))
                        {
                            // The value length limit is enforced by 
                            // MultipartBodyLengthLimit
                            var value = await streamReader.ReadToEndAsync();
                    
                            if (string.Equals(value, "undefined", 
                                StringComparison.OrdinalIgnoreCase))
                            {
                                value = string.Empty;
                            }
                    
                            formAccumulator.Append(key, value);
                    
                            if (formAccumulator.ValueCount > 
                                DefaultFormOptions.ValueCountLimit)
                            {
                                // Form key count limit of 
                                // _defaultFormOptions.ValueCountLimit 
                                // is exceeded.
                                // Log error
                                return BadRequest("The request couldn't be processed (Form key count limit is exceeded).");
                            }
                        }
                    }
                }

                // Drain any remaining section body that hasn't been consumed and
                // read the headers for the next section.
                section = await reader.ReadNextSectionAsync();
            }
            
            // Bind form data to the model
            var formValueProvider = new FormValueProvider(
                BindingSource.Form,
                new FormCollection(formAccumulator.GetResults()),
                CultureInfo.CurrentCulture);
            var bindingSuccessful = await TryUpdateModelAsync(formData, prefix: "",
                valueProvider: formValueProvider);

            if (!bindingSuccessful)
            {
                // Log error
                return BadRequest("The request couldn't be processed (Model binding error).");
            }

            if (formData.Content == null || string.IsNullOrWhiteSpace(formData.ProvidedFileName))
            {
                return BadRequest("The file and it's name cannot bind.");
            }

            var isFileClean = await _scanService.Scan(formData.Content);

            if (!isFileClean)
            {
                // Log error
                return BadRequest("The request couldn't be processed (Virus or malware).");
            }

            var fileMetaData = await _fileUploadService.UploadFileAsync(formData);
            return Created(nameof(FileController), fileMetaData);
        }

        [HttpPost("[action]")]
        public async Task<ActionResult> GetPaginated(PaginationRequestModel model)
        {
            try
            {
                var response = await _fileUploadService.GetPaginated(model);
                return Ok(response);
            }
            catch (Exception e)
            {
                return StatusCode(500, e.Message);
            }
            
        }
    }
}